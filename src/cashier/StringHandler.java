package cashier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

public class StringHandler {
	
public String sl(String local){
        
        String baseName = "resources.locals";

        try
        {
            ResourceBundle bundle = ResourceBundle.getBundle( baseName );
            local = bundle.getString(local);
        }
        catch ( MissingResourceException e ) {
            System.out.println("sl: "+ e );
        }
        
        return local;
    }


public final String ssl(String setting){
    
    Properties prop = new Properties();
    InputStream in = this.getClass().getResourceAsStream("config.properties");

    try {
        prop.load(in);
        setting = prop.getProperty(setting);
    } catch (IOException ex) {
        System.out.println("ssl:   " + ex.getMessage());
    }
    
    return setting;
    }

public final void ssw(String setting, String value){
    
    Properties prop = new Properties();
    InputStream in = this.getClass().getResourceAsStream("config.properties");
    File f = new File("config.properties");
    
    	try {
    		prop.load(in);
    		prop.setProperty(setting,value);
    		OutputStream out = new FileOutputStream(f);
    		prop.store(out, "config.properties");
    	} 
    	catch (IOException ex) {
    		System.out.println("ssw:   " + ex.getMessage());
    	}
    }

}
