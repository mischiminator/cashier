
package cashier;

/**
 *
 * @version 0.6
 * @author mischafuerst
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;



public class DBC{
    
	StringHandler SH = new StringHandler();
	
    DefaultTableModel model;

    String  u,p,s;

    public DBC(String user, String pass, String daba) throws Exception {

        u = user;
        p = pass;
        s = daba;
        
        String driver = "com.mysql.jdbc.Driver";
        
        Class.forName(driver);
        
  }
    public void productAdder(String name, String id, String price) throws Exception{

        Connection conn = DriverManager.getConnection(s,u,p);

        try{

        PreparedStatement add = conn.prepareStatement("insert into products values (?, ?, ?);");
        add.setString(1, id);
        add.setString(2, name);
        add.setString(3, price);
        add.addBatch();
        
        conn.setAutoCommit(false);
        add.executeBatch();
        conn.setAutoCommit(true);
        
        conn.close();
        while(!conn.isClosed()){
            
        }
       }catch(SQLException e){
           conn.close();
           System.out.println("productadder:  " + e);
       }
    }
    
    public DefaultTableModel productListUpdater(JTable table)throws Exception{
        
        
        Connection conn = DriverManager.getConnection(s,u,p);

        try{
        model = new DefaultTableModel();

        Statement stat = conn.createStatement();

        String SQL = "SELECT * FROM products ORDER BY p_id DESC;";
        ResultSet rs = stat.executeQuery(SQL);
        
        String id, nr, pr;
        
        model.addColumn(SH.sl("productID"));
        model.addColumn(SH.sl("productName"));
        model.addColumn(SH.sl("productPrice"));
        while(rs.next())
        { 
            id  = rs.getString("p_id");
            nr   = rs.getString("name");
            pr   = rs.getString("price");
            //Object[][]data={{id,n,p}};
            // This will add row from the DB as the last row in the JTable. 
            model.insertRow(table.getRowCount(), new Object[] {id, nr, pr});
        }       
        conn.close();
        
        }catch(SQLException abc){
            conn.close();
            System.out.println("sqlite:   "+abc.getMessage());
        }
        return model;
    }
    
    public void productRemover(String id){
        
        try{
        Connection conn = DriverManager.getConnection(s,u,p);

        Statement stat = conn.createStatement();

        String SQL = "DELETE FROM products WHERE p_id='" + id + "';";
        stat.executeUpdate(SQL);
        conn.close();
        }catch(SQLException sqle){
            System.out.println("rmv:  "+sqle.getMessage());
        }
    }
    
    public String[] productUpdater(String ID) throws Exception{
        String[] Result = new String[3];
        Connection conn = DriverManager.getConnection(s,u,p);
        try{
        
            Statement stat = conn.createStatement();

            String SQL1 = "SELECT * FROM products WHERE p_id=" + ID +";";
            String SQL2 = "DELETE FROM products WHERE p_id=" + ID +";";
            ResultSet rs = stat.executeQuery(SQL1);
            while(rs.next())
            { 
                Result[0]  = rs.getString("p_id");
                Result[1]  = rs.getString("name");
                Result[2]  = rs.getString("price");
            }
            stat.executeUpdate(SQL2);
            conn.close();
            
        }catch(SQLException e){
            conn.close();
        }
        return Result;
    }
}
