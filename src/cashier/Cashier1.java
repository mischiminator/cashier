
package cashier;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

/**
 *
 * 
 * @version 0.6
 * @author mischafuerst
 */
public class Cashier1 extends JFrame implements ActionListener{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    StringHandler SH = new StringHandler();
	DefaultTableModel model = new DefaultTableModel();
    
    JButton     	b_products, b_orders, b_cashier, b_settings,
                	add, update, remove,
                	addProduct, cancel,
                	removeProduct,
                	updateProduct;
    JPanel      	productPanel,cashierPanel,orderPanel, settingsPanel, mainPanel,
                	lsbpp, rsbpp, 
                	paf	= new JPanel(new GridLayout(10,1)),
                	prf = new JPanel(new GridLayout(10,1)), 
                	puf = new JPanel(new GridLayout(10,1));
    JScrollPane 	plsp;
    JTable      	productList;
    JLabel      	l_productName, l_productPrice, l_productID,
                	l_toRemove;
                
    JTextField  	tf_productName, tf_productPrice, tf_productID;
    JFrame      	f       = new JFrame();
    
    static      	final Dimension SIZE = new Dimension(700, 500);
    static		    final Dimension TF_SIZE = new Dimension(60, 30);
    
    String      	user, pass, daba;
    boolean     	p_loaded = false;

    public Cashier1(){
        
        user = SH.ssl("username");
        pass = SH.ssl("password");
        daba = SH.ssl("database");
        
        f.setBounds(0,0,700,555);
        f.setResizable(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().setLayout(new BorderLayout(5,5));
        
        paf.setName("paf");
        
        prf.setName("prf");
        
        puf.setName("puf");
        
        ProductRightSidebar();
        
        mainPanel       = new JPanel(new GridLayout(1,4));
        productPanel    = new JPanel(new BorderLayout(10, 10));
        orderPanel      = new JPanel(new BorderLayout(10, 10));
        cashierPanel    = new JPanel(new BorderLayout(10, 10));
        settingsPanel	= new JPanel(new BorderLayout(10, 10));
        lsbpp           = new JPanel(new GridLayout(3, 1));
        rsbpp			= new JPanel();
        
        mainPanel();
        productPanel();
        
        pack();
        f.add(mainPanel, BorderLayout.NORTH);
        f.setVisible(true);
        
    }
    
    private void mainPanel(){
        
        b_products  = new JButton(SH.sl("products"));
        b_products  .addActionListener(this);
        b_products	.setFocusable(false);
        
        b_orders    = new JButton(SH.sl("orders"));
        b_orders    .addActionListener(this);
        b_orders	.setFocusable(false);
        
        
        b_cashier   = new JButton(SH.sl("cashier"));
        b_cashier   .addActionListener(this);
        b_cashier   .setEnabled(false);
        b_cashier	.setFocusable(false);
        
        b_settings	= new JButton(SH.sl("settings"));
        b_settings	.addActionListener(this);
        b_settings	.setFocusable(false);
    
        mainPanel.add(b_products);
        mainPanel.add(b_orders);
        mainPanel.add(b_cashier);
        mainPanel.add(b_settings);
        
    }
    
    private void productPanel(){
        
        add = new JButton(SH.sl("add")); 
        add.addActionListener(this);
        
        remove = new JButton(SH.sl("remove"));
        remove.addActionListener(this);
        
        update = new JButton(SH.sl("update"));
        update.addActionListener(this);
        
        try{
        productList = new JTable(new DBC(user,pass,daba)
                        .productListUpdater(new JTable()));
        }catch(Exception e){
            System.out.println("pl:  " + e);
        }
        productList.setShowGrid(true);
        productList.setGridColor(Color.GRAY);
        productList.setEnabled(false);
        
        
        lsbpp.add(add);
        lsbpp.add(remove);
        lsbpp.add(update);
        plsp = new JScrollPane(productList);
        productPanel.add(plsp);
        productPanel.add(lsbpp, BorderLayout.WEST);
        productPanel.add(rsbpp, BorderLayout.EAST);
        productPanel.add(new JPanel(), BorderLayout.SOUTH);
    }
    
    private void ProductRightSidebar(){
        
        addProduct = new JButton(SH.sl("add"));
        addProduct.addActionListener(this);
        
        cancel = new JButton(SH.sl("cancel"));
        cancel.addActionListener(this);
        
        l_productName   = new JLabel(SH.sl("productName"));
        l_productID     = new JLabel(SH.sl("productID"));
        l_productPrice  = new JLabel(SH.sl("productPrice"));
        
        tf_productName  = new JTextField();
        tf_productName.setPreferredSize(TF_SIZE);
        tf_productID    = new JTextField();
        tf_productID.setPreferredSize(TF_SIZE);
        tf_productPrice = new JTextField();
        tf_productPrice.setPreferredSize(TF_SIZE);
        
        updateProduct   = new JButton(SH.sl("load"));
        updateProduct   .addActionListener(this);
        
        removeProduct   = new JButton(SH.sl("remove"));
        removeProduct   .addActionListener(this);
        
        l_toRemove      = new JLabel(SH.sl("toRemove"));
        l_toRemove      .setHorizontalAlignment(SwingConstants.CENTER);
        
    }
    
    private void fs(JPanel enable){
        if(enable.getName()=="prf"){
            enable.add(l_toRemove);
            enable.add(tf_productID);
            enable.add(removeProduct);
            enable.add(cancel);
        }
        if(enable.getName()=="paf"){
            
            enable.add(l_productID);
            enable.add(tf_productID);
            enable.add(l_productName);
            enable.add(tf_productName);
            enable.add(l_productPrice);
            enable.add(tf_productPrice);
            enable.add(addProduct);
            enable.add(cancel);
        }
        if(enable.getName().equals("puf")){
            enable.add(l_productPrice);
            enable.add(l_productID);
            enable.add(tf_productID);
            enable.add(l_productName);
            enable.add(tf_productName);
            enable.add(l_productPrice);
            tf_productName.setEnabled(false);
            enable.add(tf_productPrice);
            tf_productPrice.setEnabled(false);
            enable.add(updateProduct);
            enable.add(cancel);

        }
        enable.repaint();
        enable.setVisible(true);
        
    }
    
    public void ps(JPanel enable){
        f.remove(productPanel);
        f.remove(orderPanel);
        f.remove(cashierPanel);
        f.remove(settingsPanel);
        f.add(enable);
        enable.setPreferredSize(SIZE);
        f.pack();
        f.validate();
        f.repaint();
    }
    
    private void cc(){
        tf_productID   .setText("");
        tf_productName .setText("");
        tf_productName .setEnabled(true);
        tf_productPrice.setText("");
        tf_productPrice.setEnabled(true);
        paf.setVisible(false);
        prf.setVisible(false);
        puf.setVisible(false);
        repaint();
    }
    
    private void productUpdate(){
        String Result[] = new String[3];
        try{
            Result = new DBC(user,pass,daba)
                    .productUpdater(tf_productID.getText());
        }catch(Exception e){
            
        }
        tf_productName.setText(Result[1]);
        tf_productName.setEnabled(true);
        
        tf_productPrice.setText(Result[2]);
        tf_productPrice.setEnabled(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==b_products){
            b_products  .setEnabled(false);
            b_cashier   .setEnabled(true);
            b_orders    .setEnabled(true);
            b_settings	.setEnabled(true);
            
            f.repaint();
            ps(productPanel);
        }
        if(e.getSource() == b_orders)   {
            b_products  .setEnabled(true);
            b_cashier   .setEnabled(true);
            b_orders    .setEnabled(false);
            b_settings	.setEnabled(true);
            ps(orderPanel);
        }
        if(e.getSource() == b_cashier)  {
            b_products  .setEnabled(true);
            b_cashier   .setEnabled(false);
            b_orders    .setEnabled(true);
            b_settings	.setEnabled(true);
            ps(cashierPanel);
        }
        if(e.getSource() == b_settings){
        	b_products  .setEnabled(true);
            b_cashier   .setEnabled(true);
            b_orders    .setEnabled(true);
            b_settings	.setEnabled(false);
            ps(settingsPanel);
        }
        if(e.getSource() == add){
            fs(paf);
            rsbpp.removeAll();
            rsbpp.add(paf);
            f.validate();
        }
        
        if(e.getSource() == addProduct){
            try{
    
                new DBC(user,pass,daba).productAdder(
                    tf_productName.getText(),
                    tf_productID.getText(),
                    tf_productPrice.getText());
                model = new DBC(user,pass,daba).productListUpdater(new JTable());
                productList.setModel(model);

                
                tf_productID.setText("");
                tf_productName.setText("");
                tf_productPrice.setText("");
                repaint();

            }catch(Exception ex){
                System.out.println("adder:   "+ex.getMessage());
            }
            
        }
        if(e.getSource() == cancel){
            cc();
        }
        if(e.getSource() == remove){
            fs(prf);
            rsbpp.removeAll();
            rsbpp.repaint();
            rsbpp.add(prf);
            f.validate();
        }
        if(e.getSource() == removeProduct){
            
            try{
                new DBC(user,pass,daba)
                        .productRemover(tf_productID.getText());
                tf_productID.setText("");
                model = new DBC(user,pass,daba).productListUpdater(new JTable());
                productList.setModel(model);
            }catch(Exception sqle){
                System.out.println("rm:   "+sqle.getMessage());
            }
        }
        if(e.getSource() == update){
            fs(puf);
            rsbpp.removeAll();
            rsbpp.add(puf);
            f.validate();
        }
        if(e.getSource() == updateProduct){
               
            if(p_loaded == true){
                    
                try{
    
                    new DBC(user,pass,daba).productAdder(
                        tf_productName.getText(),
                        tf_productID.getText(),
                        tf_productPrice.getText());
                    model = new DBC(user,pass,daba).productListUpdater(new JTable());
                    productList.setModel(model);

                
                    tf_productID.setText("");
                    tf_productName.setText("");
                    tf_productPrice.setText("");
                    repaint();

                }catch(Exception ex){
                System.out.println("adder:   "+ex.getMessage());
                }
                updateProduct.setText(SH.sl("load"));
                tf_productName.setEnabled(false);
                tf_productPrice.setEnabled(false);
                p_loaded = false;
                return;
            }
        
            if(p_loaded == false){
                productUpdate();
                updateProduct.setText(SH.sl("save"));
                p_loaded = true;
            }
        }
    }
    
    
    

    
    public static void main(String[] args) throws Exception{
        
    	new Cashier1();
        
    }
}
